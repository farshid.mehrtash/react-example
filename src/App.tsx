import React, {useEffect, useState} from 'react';
import './App.css';
import MyAlert from "./componenets/MyAlert";
import CatFact from "./componenets/CatFact";
import LoginComponent from "./componenets/GotPhoto/LoginComponent";
import GetJobsComponent from "./componenets/GotPhoto/GetJobsComponenet";

function App() {

    const [showError, setShowError] = useState<boolean>(false);

    const [isLogged, setIsLogged] = useState<boolean>(false);

    const [accessToken, setAccessToken] = useState<string>("");

    const toggleError = () => {
        setShowError(!showError);
    };

    const onLoginSuccessHandler = (accessToken: string) => {
        localStorage.setItem("accessToken", accessToken);
        setAccessToken(accessToken);
    }

    useEffect(() => {
        setAccessToken( localStorage.getItem("accessToken") || "")
        //setIsLogged(localStorage.getItem("accessToken") ? true : false);
        setIsLogged(!!localStorage.getItem("accessToken"));
    },[]);

    return (
        <div className="App">

            {/*<MyAlert message={"Hello World"} title={"First"} showLink={true} isError={showError}/>
            <br/>
            <br/>
            <button className={"bg-blue-600"} onClick={toggleError}> Click Me!</button>

            <hr/>*/}

            {!isLogged && (
                <LoginComponent onLoginSuccess={onLoginSuccessHandler}/>
            )}

            <LoginComponent onLoginSuccess={onLoginSuccessHandler}/>

            <hr/>
            <br/>
            <br/>

            {isLogged && (
                <GetJobsComponent accessToken={ accessToken }/>
            )}

            {/*<hr/>
            <hr/>
            <div className={"bg-gray-400 hover:bg-red-600 transition-all duration-1000 hover:text-white"} style={{height: "200px", width: "200px"}}>
                Hello World
            </div>
            <hr/>
            <div className={"bg-blue-600 group"}>
                <p className={"text-blue-700"}> Click for More </p>
                <p className={" text-blue-700 group-hover:text-white "}> Click for More2 </p>
            </div>

            <MyAlert message={"Good Bye"} title={"Break"}/>*/}
        </div>
    );
}

export default App;
