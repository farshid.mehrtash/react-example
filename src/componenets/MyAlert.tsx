import React from "react";

interface MyAlertProps {
    message: string;
    title: string;
    showLink?: boolean;
    isError?: boolean;
}


const MyAlert: React.FC<MyAlertProps> = ({message, title, showLink, isError = false}) => {

    const backgroundClass = isError ? 'bg-red-600 border-red-600' : 'bg-amber-600 border-amber-600';

    return (
        <div className={`${backgroundClass}   border-1 rounded p-2`}>
            <h1>{title}</h1>
            {message}
            <hr/>
            {showLink && <a href={"https://www.google.com"}>Click Here</a>}

            {showLink && (
                <>
                    <a href={"https://www.google.com"}>Click Here</a>
                    <a href={"https://www.google.com"}>Click Here2</a>
                </>
            )}

        </div>
    );

};

export default MyAlert;