import React, {useEffect} from "react";
import axios from "axios";

interface CatFactProps {

}

const CatFact: React.FC<CatFactProps> = () => {

    const [catFact, setCatFact] = React.useState<string>("Loading...");
    const [initialLoad, setInit ] = React.useState<boolean>(false);

    const getCatFactAPI = () => {
        const endpoint = "https://catfact.ninja/fact";
        const response = axios.get(endpoint)
        response.then((data) => {
            console.log(data.data.fact)
            setCatFact(data.data.fact);
        })
    }

    useEffect(() => {
        console.log("Initial Load: ", initialLoad)
        getCatFactAPI();

    }, [initialLoad] );

    return (
        <div>
            {catFact}
            <hr/>
            <button onClick={ ()=>{
                setInit(!initialLoad)
            } }>
                Toggle init
            </button>
        </div>
    )
}

export default CatFact;
