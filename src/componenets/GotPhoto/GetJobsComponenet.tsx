import React, {useEffect} from "react";
import axios from "axios";

interface GetJobsComponentProps {
    accessToken: string;
}

interface Job {
    name: string;
    id: number;
}

const GetJobsComponent: React.FC<GetJobsComponentProps> = ({accessToken}) => {

    const [jobList, setJobList] = React.useState <Job[] | undefined>();

    const getJobs = async () => {
        try {
            const response = await axios({
                method: 'GET',
                url: 'https://api.getphoto.io/v4/jobs',
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                    Accept: 'application/json',
                },
            });

            return response.data;
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getJobs().then((data) => {
            setJobList(data);
            console.log("Data: ", data)
        });
    }, [accessToken]);

    return (
        <div>

            {jobList ? (
                <ul>
                    {jobList.map((job) =>
                        <li> {job.name} </li>
                    )}
                </ul>
            ) : (
                <p> Loading... </p>
            )}

        </div>
    );
}

export default GetJobsComponent;





