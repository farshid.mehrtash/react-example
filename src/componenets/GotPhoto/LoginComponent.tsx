import React, {useEffect} from "react";
import axios from "axios";

interface LoginComponentProps {
    onLoginSuccess: (accessToken: string) => void;
}

const LoginComponent: React.FC<LoginComponentProps> = ({onLoginSuccess}) => {

    const [username, setUsername] = React.useState<string>("");
    const [password, setPassword] = React.useState<string>("");

    const [passwordLength, setPasswordLength] = React.useState<number>(0);

    const handleUsernameChange = (event: any) => {
        setUsername(event.target.value);
    }

    const handlePasswordChange = (event: any) => {
        setPassword(event.target.value);
    }

    const handleSubmit = async (event: any) => {
        event.preventDefault();
        try {
            const response = await axios({
                method: 'post',
                url: 'https://auth.getphoto.io/oauth/access_token',
                data: {
                    username: username,
                    password: password,
                    grant_type: 'password',
                    client_id: 'CLIENT ID',
                    scope: 'core.jobs.read,core.jobs.write',
                    client_secret: 'CLIENT SECRET',
                }
            });
            const accessToken = response.data.access_token;
            onLoginSuccess(accessToken);

        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {

        setPasswordLength(password.length)

    }, [password]);

    return (

        <div>
            <form onSubmit={handleSubmit}>
                <h1>Login</h1>
                <input type="text" name="username" className={"border border-amber-600 shadow"} onChange={handleUsernameChange}/>
                <hr/>
                <input type="password" name="password" className={"border border-amber-600 shadow"} onChange={handlePasswordChange}/>
                <hr/>
                <button type="submit" className={"border border-amber-600 shadow"}> Login</button>
            </form>
            <div>{passwordLength}</div>
        </div>


    );


}

export default LoginComponent;





